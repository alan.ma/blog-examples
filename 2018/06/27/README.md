You know the drill, build me like:

```
docker build -t cvs2git -f Dockerfile  .
```

Then use me like (stolen instructions from [Sourceforge](https://sourceforge.net/p/forge/documentation/CVS/), kind of)

```
docker run -v /path/to/your/actual/cvs/repo:/cvs cvs2git --blobfile=blob.dat --dumpfile=dump.dat --username=FIXME --default-eol=native --encoding=utf8 --encoding=latin1 --fallback-encoding=ascii /cvs

# Using the container ID do something like (assuming the container ID is c9f422c5202

docker cp c9f422c52024:/blob.dat .
docker cp c9f422c52024:/dump.dat .

mkdir $PWD/html2php/ && cd $PWD/html2php/ && git init && cd -

cat blob.dat dump.dat | git --git-dir=$PWD/html2php/fast-import
```

Make sure you create your Git repo using the Admin tools on Sourceforce and you correcly add the remote repo

![alt text](git.png)
