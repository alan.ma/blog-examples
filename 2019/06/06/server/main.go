package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/google/uuid"
	jose "gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
)

const (
	defaultPort = 9876
	openIDURL   = `https://login.microsoftonline.com/%s/.well-known/openid-configuration` // %s=tenant
)

type (
	appConfig struct {
		Tenant        string
		ApplicationID string
		GUIDs         string
	}

	// OpenID Connect metadata document
	openIDMetadataDocument struct {
		AuthorizationEndpoint string `json:"authorization_endpoint"`
		EndSessionEndpoint    string `json:"end_session_endpoint"`
		JWKSURI               string `json:"jwks_uri"`
	}

	// Response returned as a POST after signing in
	signInResponse struct {
		State            string `json:"-"`
		Token            string `json:"token,omitempty"`
		Error            string `json:"error,omitempty"`
		ErrorDescription string `json:"error_description,omitempty"`
	}

	// https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-token-and-claims#claims-in-idtokens
	activeDirectoryClaims struct {
		*jwt.Claims
		Nonce        string   `json:"nonce"`
		UPN          string   `json:"upn"`
		GivenName    string   `json:"given_name"`
		FamilyName   string   `json:"family_name"`
		NotBefore    int64    `json:"nbf"`
		NotOnOrAfter int64    `json:"exp"`
		IssuedAt     int64    `json:"iat"`
		Groups       []string `json:"groups"`
	}

	personalInformation struct {
		Name        string
		PhoneNumber string
	}
)

var (
	cachedJWKSet    *jose.JSONWebKeySet
	cachedOpenIDDoc = openIDMetadataDocument{}
	config          = appConfig{}
	sessions        = map[string]struct{}{}
	guids           = map[string]struct{}{}
)

func main() {
	flag.StringVar(&config.Tenant, "tenant", "", "Azure Tenant")
	flag.StringVar(&config.ApplicationID, "appid", "", "Azure Application ID")
	flag.StringVar(&config.GUIDs, "guids", "", "Comma Separated Azure Group IDs")
	flag.Parse()

	if config.Tenant == "" || config.ApplicationID == "" { // XXX disabling GUIs for blog post || config.GUIDs == "" {
		log.Fatal("tenant, appid and guids are required")
	}
	splittedGuids := strings.Split(config.GUIDs, ",")
	for _, guid := range splittedGuids {
		guids[guid] = struct{}{}
	}

	h := http.NewServeMux()
	h.HandleFunc("/login", loginHandler)
	h.HandleFunc("/logout", logoutHandler)
	h.HandleFunc("/token", tokenHandler)
	h.HandleFunc("/information", privateMiddleware(informationHandler))

	h.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintln(w, "Resource not found")
	})

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", defaultPort), h))
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	if ok := expectedMethod(w, r.Method, http.MethodGet); !ok {
		return
	}

	nonce, state, err := newUUIDs()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if err := initOpenIDDocument(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	req, err := http.NewRequest(http.MethodGet, cachedOpenIDDoc.AuthorizationEndpoint, nil)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	q := req.URL.Query()
	q.Add("client_id", config.ApplicationID)
	q.Add("response_type", "id_token")
	q.Add("redirect_uri", fmt.Sprintf("http://localhost:%d/token", defaultPort))
	q.Add("response_mode", "form_post")
	q.Add("scope", "openid")
	q.Add("nonce", nonce.String())
	q.Add("state", state.String())
	req.URL.RawQuery = q.Encode()

	// FIXME Primitive way to mitigate token replay attacks, use a real data store
	sessions[fmt.Sprintf("%s_%s", nonce.String(), state.String())] = struct{}{}

	http.Redirect(w, r, req.URL.String(), http.StatusFound)
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
	if ok := expectedMethod(w, r.Method, http.MethodGet); !ok {
		return
	}

	if err := initOpenIDDocument(); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, cachedOpenIDDoc.EndSessionEndpoint, http.StatusFound)
}

func tokenHandler(w http.ResponseWriter, r *http.Request) {
	if ok := expectedMethod(w, r.Method, http.MethodPost); !ok {
		return
	}

	err := r.ParseForm()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	payload := signInResponse{
		State:            r.Form.Get("state"),
		Token:            r.Form.Get("id_token"),
		Error:            r.Form.Get("error"),
		ErrorDescription: r.Form.Get("error_description"),
	}

	if payload.Error != "" {
		respondWithJSON(w, payload, http.StatusBadRequest)
		return
	}

	var adClaims *activeDirectoryClaims
	if adClaims, err = parseJWT(payload.Token); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// FIXME Primitive way to mitigate token replay attacks, use a real data store
	_, ok := sessions[fmt.Sprintf("%s_%s", adClaims.Nonce, payload.State)]
	if !ok {
		http.Error(w, "Login token has expired, log in again", http.StatusInternalServerError)
		return
	}
	delete(sessions, fmt.Sprintf("%s_%s", adClaims.Nonce, payload.State))

	respondWithJSON(w, payload, http.StatusOK)
}

func informationHandler(w http.ResponseWriter, r *http.Request) {
	respondWithJSON(w,
		personalInformation{
			Name:        "Juan Perez",
			PhoneNumber: "123-456-7890",
		},
		http.StatusOK)
}

func privateMiddleware(next func(http.ResponseWriter, *http.Request)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		if ok := expectedMethod(w, r.Method, http.MethodGet); !ok {
			return
		}

		authHeader := r.Header.Get("Authorization")
		if authHeader == "" {
			http.Error(w, "Authorization header missing", http.StatusBadRequest)
			return
		}

		values := strings.Split(authHeader, " ")
		if len(values) != 2 || values[0] != "Bearer" {
			http.Error(w, "Authorization header value is invalid", http.StatusBadRequest)
			return
		}

		_, err := parseJWT(values[1])
		// adClaims, err := parseJWT(values[1]) // XXX Originall we were doing this
		if err != nil {
			fmt.Println(err.Error())
			http.Error(w, fmt.Sprintf("Access Denied: %s", err.Error()), http.StatusUnauthorized)
			return
		}

		/*
			// XXX Originally we were doing this
			found := 0
			for _, group := range adClaims.Groups {
				if _, ok := guids[group]; ok {
					found++
					if found == len(guids) {
						break
					}
				}
			}

			if found != len(guids) {
				http.Error(w, "Access Denied", http.StatusUnauthorized)
				return
			}*/

		next(w, r)
	}
}

func newUUIDs() (uuid.UUID, uuid.UUID, error) {
	nonce, err := uuid.NewUUID()
	if err != nil {
		return uuid.UUID{}, uuid.UUID{}, err
	}

	state, err := uuid.NewUUID()

	return nonce, state, nil
}

func initOpenIDDocument() error {
	// FIXME Mutex this
	// FIXME Bust cache accordingly
	if cachedOpenIDDoc.AuthorizationEndpoint == "" {
		if err := decodeRequest(fmt.Sprintf(openIDURL, config.Tenant), &cachedOpenIDDoc); err != nil {
			return fmt.Errorf("error decoding OpenID request: %s", err)
		}
	}
	return nil
}

func requestKeysSet() (*jose.JSONWebKeySet, error) {
	// FIXME Mutex this
	if err := initOpenIDDocument(); err != nil {
		return nil, fmt.Errorf("error decoding OpenID URL")
	}

	if cachedJWKSet != nil {
		return cachedJWKSet, nil // FIXME add actual cache-busting logic
	}

	cachedJWKSet = &jose.JSONWebKeySet{}
	if err := decodeRequest(cachedOpenIDDoc.JWKSURI, cachedJWKSet); err != nil {
		return nil, fmt.Errorf("error decoding JWKS URL")
	}

	return cachedJWKSet, nil
}

func decodeRequest(url string, v interface{}) (err error) {
	var req *http.Request
	if req, err = http.NewRequest(http.MethodGet, url, nil); err != nil {
		return err
	}

	var resp *http.Response
	var client http.Client
	if resp, err = client.Do(req); err != nil {
		return err
	}
	defer func() {
		if err = resp.Body.Close(); err != nil {
			fmt.Fprintf(os.Stderr, "there was an error closing body %s\n", err)
		}
	}()

	return json.NewDecoder(resp.Body).Decode(v)
}

func respondWithJSON(w http.ResponseWriter, payload interface{}, code int) {
	response, err := json.Marshal(payload)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	if _, err := w.Write(response); err != nil {
		fmt.Fprintf(os.Stderr, "Received error %s\n", err)
	}
}

func parseJWT(token string) (*activeDirectoryClaims, error) {
	var jwkSet *jose.JSONWebKeySet
	var err error
	if jwkSet, err = requestKeysSet(); err != nil {
		return nil, fmt.Errorf("error requesting JWK Set %s", err)
	}

	var jwtSigned *jwt.JSONWebToken
	if jwtSigned, err = jwt.ParseSigned(token); err != nil {
		return nil, err
	}

	var kid string
	for _, header := range jwtSigned.Headers {
		if kid == "" {
			kid = header.KeyID
			break
		}
	}

	jsonKey := jwkSet.Key(kid)

	claimsMap := make(map[string]interface{})
	claims := activeDirectoryClaims{}
	if err = jwtSigned.Claims(jsonKey[0].Key, &claims, &claimsMap); err != nil {
		return nil, fmt.Errorf("error getting claims: %s", err)
	}

	return &claims, nil
}

func expectedMethod(w http.ResponseWriter, received, expected string) bool {
	if received == expected {
		return true
	}

	http.Error(w, fmt.Sprintf("Invalid method %s", received), http.StatusBadRequest)
	return false
}
