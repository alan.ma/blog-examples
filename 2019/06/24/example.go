package counterfeiter

//go:generate counterfeiter -o fake/writer.gen.go -fake-name Writer . Writer

type Writer interface {
	Write() error
}

func WriteSomething(w Writer) bool {
	if err := w.Write(); err != nil {
		return false
	}
	return true
}
