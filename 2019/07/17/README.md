# `ifacecodegen` example

1. `go get github.com/fredipevcin/ifacecodegen/cmd/ifacecodegen`
1. `go generate .`
1. `go build -o ifacecodegen-example gitlab.com/MarioCarrion/blog-examples/2019/07/17 && ./ifacecodegen-example`
