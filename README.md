Hello person, you're here because... you [found me](https://mariocarrion.com)? Even if you didn't I'm sharing with you all the actual _actionable_ files that represent the examples I used on my blog, the actual code is grouped by the data the blog was posted.

Have fun.

# Posts

* [2019/07/17](2019/07/17) - [Go Tool: ifacecodegen](https://www.mariocarrion.com/2019/07/17/golang-tools-ifacecodegen.html)
* [2019/06/24](2019/06/24) - [Go Tool: counterfeiter](https://www.mariocarrion.com/2019/06/24/golang-tools-counterfeiter.html)
* [2019/06/06](2019/06/06) - [Server To Server Authentication + Azure Active Directory](https://www.mariocarrion.com/2019/06/06/server-to-server-authentication-azure-active-directory.html)
* [2018/07/16](2018/07/16) - [Go Tip: Azure Active Directory + JWT](https://mariocarrion.com/2018/07/16/azure-active-directory-jwt.html)
* [2018/06/27](2018/06/27) - [I'm ready for July](https://mariocarrion.com/2018/06/27/im-ready-for-july-2018.html)
